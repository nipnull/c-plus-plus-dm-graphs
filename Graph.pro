TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11
CONFIG += console c++11

SOURCES += main.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    graph.h \
    graph.h.impl \
    vertex.h \
    vertex.h.impl \
    binomialheap.h \
    binomialheap.h.impl \
    binomailnode.h \
    binomailnode.h.impl \
    dsu.h \
    dsu.h.impl \
    flow.h \
    flow.h.impl \
    treap.h \
    treap.h.impl \
    treapnode.h \
    treapnode.h.impl \
    kmp.h \
    !_All_include.h \
    ahocorasick.h \
    ahocorasick.h.impl \
    ahocorasicknode.h \
    ahocorasicknode.h.impl

